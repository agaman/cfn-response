# Cfn Response &middot; [![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://github.com/facebook/react/blob/main/LICENSE) ![npm bundle size (scoped)](https://img.shields.io/bundlephobia/min/@fogwarts/cfn-response) ![npm (scoped)](https://img.shields.io/npm/v/@fogwarts/cfn-response) ![npm](https://img.shields.io/npm/dw/@fogwarts/cfn-response)

Cfn Response helps you in sending http responses for lambda-based custom resources in AWS CloudFormation


## Why would I need this ?

When an AWS Custom Resource is created/updated/deleted, the custom resource handler will process that specific event

After the custom resource handler finished its job (or threw an exception), a response needs to be sent back to CloudFormation, so it can update the custom resource's status accordingly

This module comes in handy when responding back to CloudFormation from the custom resource handler

It is very similar to the popular [cfn-response](https://www.npmjs.com/package/cfn-response) package in its functionality. The only things this module does differently are:
* **send** is an awaitable function, so we are sure that the request completes and our lambda is not cut off in the middle of a request
* **send** does not automatically close the context, it will not force the lambda function using this module to stop running
* **send** has a slick API, parameters are send via a request object

## Usage example

```js
import { CustomResourceResponseStatus, send } from '@fogwarts/cfn-response'

// Usage in a CloudFormationCustomResourceHandler
export const handler = async (event, context) => {
    const physicalResourceId = getPhysicalResourceId(event)

    try {
        // Do stuff

        return await send({
            status: CustomResourceResponseStatus.success,
            event, context, physicalResourceId,
            data: {}
        })
    } catch (e) {
        return await send({
            status: CustomResourceResponseStatus.failed,
            event, context, physicalResourceId,
            data: {}
        })
    }
}
```

## Module exports

### CustomResourceResponseStatus (enum)

```js
enum CustomResourceResponseStatus {
    success = 'SUCCESS',
    failed = 'FAILED'
}
```

### send (function)

```js
type CustomResourceResponseProps = {
    status: CustomResourceResponseStatus
    reason?: string;

    event: CloudFormationCustomResourceEvent,
    context: Context,
    physicalResourceId: string,

    data?: { [key: string]: any },
}

const send = async (props: CustomResourceResponseProps) => new Promise<void>
```

[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/made-with-typescript.svg)](https://forthebadge.com)
[![forthebadge](https://forthebadge.com/images/badges/powered-by-coffee.svg)](https://forthebadge.com)